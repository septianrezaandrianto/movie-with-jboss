package mov.moviejbossreza.dto;

public class DirectorDTO {
	
	private int dirId;
	private String dirFname;
	private String dirLname;
	
	public DirectorDTO() {
		super();
	}

	public DirectorDTO(int dirId, String dirFname, String dirLname) {
		super();
		this.dirId = dirId;
		this.dirFname = dirFname;
		this.dirLname = dirLname;
	}

	public int getDirId() {
		return dirId;
	}

	public void setDirId(int dirId) {
		this.dirId = dirId;
	}

	public String getDirFname() {
		return dirFname;
	}

	public void setDirFname(String dirFname) {
		this.dirFname = dirFname;
	}

	public String getDirLname() {
		return dirLname;
	}

	public void setDirLname(String dirLname) {
		this.dirLname = dirLname;
	}
	
	
	
}
