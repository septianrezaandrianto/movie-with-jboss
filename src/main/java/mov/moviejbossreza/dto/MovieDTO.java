package mov.moviejbossreza.dto;

import java.util.Date;

public class MovieDTO {

	private long movId;
	private String movTitle;
	private Integer movYear;
	private Integer movTime;
	private String movLang;
	private Date movDtRel;
	private String movRelCountry;
	
	public MovieDTO() {
		super();
	}
	
	public MovieDTO(long movId, String movTitle, Integer movYear, Integer movTime, String movLang, Date movDtRel,
			String movRelCountry) {
		super();
		this.movId = movId;
		this.movTitle = movTitle;
		this.movYear = movYear;
		this.movTime = movTime;
		this.movLang = movLang;
		this.movDtRel = movDtRel;
		this.movRelCountry = movRelCountry;
	}

	public long getMovId() {
		return movId;
	}

	public void setMovId(long movId) {
		this.movId = movId;
	}

	public String getMovTitle() {
		return movTitle;
	}

	public void setMovTitle(String movTitle) {
		this.movTitle = movTitle;
	}

	public Integer getMovYear() {
		return movYear;
	}

	public void setMovYear(Integer movYear) {
		this.movYear = movYear;
	}

	public Integer getMovTime() {
		return movTime;
	}

	public void setMovTime(Integer movTime) {
		this.movTime = movTime;
	}

	public String getMovLang() {
		return movLang;
	}

	public void setMovLang(String movLang) {
		this.movLang = movLang;
	}

	public Date getMovDtRel() {
		return movDtRel;
	}

	public void setMovDtRel(Date movDtRel) {
		this.movDtRel = movDtRel;
	}

	public String getMovRelCountry() {
		return movRelCountry;
	}

	public void setMovRelCountry(String movRelCountry) {
		this.movRelCountry = movRelCountry;
	}
	
}
