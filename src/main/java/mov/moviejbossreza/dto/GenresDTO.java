package mov.moviejbossreza.dto;

public class GenresDTO {

	private int genId;
	private String genTitle;
	
	public GenresDTO() {
		super();
	}

	public GenresDTO(int genId, String genTitle) {
		super();
		this.genId = genId;
		this.genTitle = genTitle;
	}

	public int getGenId() {
		return genId;
	}

	public void setGenId(int genId) {
		this.genId = genId;
	}

	public String getGenTitle() {
		return genTitle;
	}

	public void setGenTitle(String genTitle) {
		this.genTitle = genTitle;
	}
	
	
}
