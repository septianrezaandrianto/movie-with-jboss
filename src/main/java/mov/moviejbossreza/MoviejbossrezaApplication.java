package mov.moviejbossreza;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MoviejbossrezaApplication {

	public static void main(String[] args) {
		SpringApplication.run(MoviejbossrezaApplication.class, args);
	}

}
