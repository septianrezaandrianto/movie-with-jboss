package moviejbossreza.generateentities.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import moviejbossreza.generateentities.Actor;

@Repository
public interface ActorRepository extends JpaRepository<Actor, Integer>{

}
