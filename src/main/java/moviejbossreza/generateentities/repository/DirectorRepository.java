package moviejbossreza.generateentities.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import moviejbossreza.generateentities.Director;

@Repository
public interface DirectorRepository extends JpaRepository<Director, Integer>{

}
