package moviejbossreza.generateentities.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import moviejbossreza.generateentities.Genres;

@Repository
public interface GenresRepository extends JpaRepository<Genres, Integer> {

}
