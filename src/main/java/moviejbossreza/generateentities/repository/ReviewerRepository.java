package moviejbossreza.generateentities.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import moviejbossreza.generateentities.Reviewer;

@Repository
public interface ReviewerRepository extends JpaRepository<Reviewer, Integer>{

}
