package moviejbossreza.generateentities.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import moviejbossreza.generateentities.Movie;

@Repository
public interface MovieRepository extends JpaRepository<Movie, Integer>{

}
