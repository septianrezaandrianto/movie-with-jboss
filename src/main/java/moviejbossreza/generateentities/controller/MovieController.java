package moviejbossreza.generateentities.controller;

import java.util.ArrayList;
import java.util.HashMap;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import mov.moviejbossreza.dto.MovieDTO;
import moviejbossreza.generateentities.Movie;
import moviejbossreza.generateentities.repository.MovieRepository;

@RestController
@RequestMapping("/api")
public class MovieController {

	@Autowired
	private MovieRepository movieRepository;
	
	ModelMapper modelMapper = new ModelMapper();
	
//	Convert DTO To Entity
	public Movie convertDTOToEntity(MovieDTO movieDTO) {
		Movie mov = modelMapper.map(movieDTO,Movie.class);
	return mov;
	}
	
//	Convert Entity To DTO
	public MovieDTO convertEntityToDTO(Movie movie) {
		MovieDTO movDto = modelMapper.map(movie, MovieDTO.class);
	return movDto;
	}
	
//	Melihat list Movie
	@GetMapping("/movies/all")
	public HashMap<String, Object> getAllMovies() {
		HashMap<String, Object> hasMov = new HashMap<String, Object>();
		
		ArrayList<MovieDTO> listMovDto = new ArrayList<MovieDTO>();
		
		for (Movie mov : movieRepository.findAll()) {
			MovieDTO movDto = convertEntityToDTO(mov);
			listMovDto.add(movDto);
		}
		hasMov.put("Message", "All Data");
		hasMov.put("Total", listMovDto.size());
		hasMov.put("Data", listMovDto);
	return hasMov;
	}
	
//	Melihat movie by Id
	@GetMapping("/movies/{id}")
	public HashMap<String, Object> getMovieById(@PathVariable(value = "id") int movId) {
		HashMap<String, Object> hasMov = new HashMap<String, Object>();
		
		Movie mov = movieRepository.findById(movId).orElse(null);
		
		MovieDTO movDto = convertEntityToDTO(mov);
		
		hasMov.put("Message" , "Data By Id");
		hasMov.put("Data", movDto);
	return hasMov;
	}
	
//	Menambahkan sebuah movie
	@PostMapping("movies/add")
	public HashMap<String, Object> addMovie(@Valid @RequestBody MovieDTO movieDTO) {
		HashMap<String, Object> hasMov = new HashMap<String, Object>();
		
		Movie mov = convertDTOToEntity(movieDTO);
		
		hasMov.put("Message" , "Add success");
		hasMov.put("Data" , mov);
	return hasMov;
	}
	
//	Menghapus sebuah movie
	@DeleteMapping("movies/delete/{id}")
	public HashMap<String, Object> deleteMovieById(@PathVariable(value="id") int movId) {
		HashMap<String, Object> hasMov = new HashMap<String, Object>();
		
		Movie mov = movieRepository.findById(movId).orElse(null);
		
		movieRepository.delete(mov);
		
		hasMov.put("Message", "Delete Success");
		hasMov.put("Data", mov);
	return hasMov;
	}
	
//	Mengupdate movie
	@PutMapping("/movies/update/{id}")
	public HashMap<String, Object> updateMovie(@PathVariable(value = "id") int movId, @Valid @RequestBody MovieDTO movieDTO) {
		HashMap<String, Object> hasMov = new HashMap<String, Object>();
		
		Movie mov = movieRepository.findById(movId).orElse(null);
		
		movieDTO.setMovId(mov.getMovId());
		
		if (movieDTO.getMovTitle() != null) {
			mov.setMovTitle(convertDTOToEntity(movieDTO).getMovTitle());
		}
		
		if (movieDTO.getMovYear() != null) {
			mov.setMovYear(convertDTOToEntity(movieDTO).getMovYear());
		}
		
		if (movieDTO.getMovTime() != null) {
			mov.setMovTime(convertDTOToEntity(movieDTO).getMovTime());
		}
		
		if (movieDTO.getMovRelCountry() != null) {
			mov.setMovRelCountry(convertDTOToEntity(movieDTO).getMovRelCountry());
		}
		
		if (movieDTO.getMovLang() != null) {
			mov.setMovLang(convertDTOToEntity(movieDTO).getMovLang());
		}
		
		if (movieDTO.getMovDtRel() != null) {
			mov.setMovDtRel(convertDTOToEntity(movieDTO).getMovDtRel());
		}
		
		hasMov.put("Message", "Update Success");
		hasMov.put("Data" , movieRepository.save(mov));
	return hasMov;
	}
}
