package moviejbossreza.generateentities.controller;

import java.util.ArrayList;
import java.util.HashMap;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import mov.moviejbossreza.dto.ActorDTO;
import moviejbossreza.generateentities.Actor;
import moviejbossreza.generateentities.repository.ActorRepository;

@RestController
@RequestMapping("/api")
public class ActorController {

	@Autowired
	private ActorRepository actorRepository;
	
	ModelMapper modelMapper = new ModelMapper();
	
//	Convert Entity To DTO
	private ActorDTO convertEntityToDTO(Actor actor) {
		ActorDTO actDto = modelMapper.map(actor, ActorDTO.class);
	return actDto;
	}
	
//	Convert DTO To Entity
	private Actor convertDTOToEntity(ActorDTO actorDTO) {
		Actor act = modelMapper.map(actorDTO, Actor.class);
	return act;
	}
	
//	Melihat semua actor
	@GetMapping("/actors/all")
	public HashMap<String, Object> getAllActors () {
		
		HashMap<String, Object> hashmapActor = new HashMap<String, Object>();
		ArrayList<ActorDTO> listActorDto = new ArrayList<ActorDTO>();
		
		for (Actor act : actorRepository.findAll()) {
			
			ActorDTO actDto = convertEntityToDTO(act);
			listActorDto.add(actDto);
		}
		
		hashmapActor.put("Message", "All Data");
		hashmapActor.put("Total", listActorDto.size());
		hashmapActor.put("Data" , listActorDto);
		
	return hashmapActor;
	}
	
//	Melihat actor berdasarkan id
	@GetMapping("/actors/{id}")
	public HashMap<String, Object> getActorById(@PathVariable(value="id") int actId) {
		
		HashMap<String, Object> hashmapActor = new HashMap<String, Object>();
		
		Actor act = actorRepository.findById(actId).orElse(null);
						// Mapping dengan model mapper	
		ActorDTO actDto = convertEntityToDTO(act);
		
		hashmapActor.put("Message" , "Data By Id");
		hashmapActor.put("Data", actDto);
		
	return hashmapActor;
	}
	
//	Menambahkan seorang actor
	@PostMapping("/actors/add")
	public HashMap<String, Object> addActor(@Valid @RequestBody ActorDTO actorDTO) {
		
		HashMap<String, Object> hashmapActor = new HashMap<String, Object>();
					//Mapping dengan model mapper	
		Actor act = convertDTOToEntity(actorDTO);
		
		hashmapActor.put("Message", "Add success");
		hashmapActor.put("Data", actorRepository.save(act));
		
	return hashmapActor;
	}
	
//	Menghapus actor
	@DeleteMapping("/actors/delete/{id}")
	public HashMap<String, Object> deleteActorById(@PathVariable(value="id") int actId) {
		
		HashMap<String, Object> hashmapActor = new HashMap<String, Object>();
		
		Actor act = actorRepository.findById(actId).orElse(null);
		
		actorRepository.delete(act);
		
		hashmapActor.put("Message", "Delete success");
		hashmapActor.put("Data",act);
		
	return hashmapActor;
	}
	
//	Update data actors
	@PutMapping("/actors/update/{id}")
	public HashMap<String, Object> updateActor(@PathVariable(value="id") int actId, @Valid @RequestBody ActorDTO actorDTO) {
		
		HashMap<String, Object> hashmapActor = new HashMap<String, Object>();
		
		Actor act = actorRepository.findById(actId).orElse(null);
		
		actorDTO.setActId(act.getActId());
		
		//	Mapping dengan model mapper
		if(actorDTO.getActFname() != null) {
			act.setActFname(convertDTOToEntity(actorDTO).getActFname());
		}
		if(actorDTO.getActLname() != null) {
			act.setActLname(convertDTOToEntity(actorDTO).getActLname());
		}
		if(actorDTO.getActGender() != 0) {
			act.setActGender(convertDTOToEntity(actorDTO).getActGender());
		}
		
		hashmapActor.put("Message", "Update success");
		hashmapActor.put("Data", actorRepository.save(act));
		
	return hashmapActor;
	}
}
