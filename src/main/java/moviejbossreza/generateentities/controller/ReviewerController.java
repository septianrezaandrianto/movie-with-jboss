package moviejbossreza.generateentities.controller;

import java.util.ArrayList;
import java.util.HashMap;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import mov.moviejbossreza.dto.ReviewerDTO;
import moviejbossreza.generateentities.Reviewer;
import moviejbossreza.generateentities.repository.ReviewerRepository;

@RestController
@RequestMapping("/api")
public class ReviewerController {
	
	@Autowired
	private ReviewerRepository reviewerRepository;
	
	ModelMapper modelMapper = new ModelMapper();
	
//	Convert Entity To DTO
	public ReviewerDTO convertEntityToDTO(Reviewer reviewer) {
		ReviewerDTO revDto = modelMapper.map(reviewer, ReviewerDTO.class);
	return revDto;
	}
	
//	Convert DTO To Entity
	public Reviewer convertDTOToEntity(ReviewerDTO reviewerDTO) {
		Reviewer rev = modelMapper.map(reviewerDTO, Reviewer.class);
	return rev;
	}
	
//	Melihat list Reviewer
	@GetMapping("/reviewers/all")
	public HashMap<String, Object> getAllReviewers() {
		HashMap<String, Object> hasRev = new HashMap<String, Object>();
		ArrayList<ReviewerDTO> listRevDto = new ArrayList<ReviewerDTO>();
		
		for (Reviewer rev : reviewerRepository.findAll()) {
			ReviewerDTO revDto = convertEntityToDTO(rev);
			listRevDto.add(revDto);
		}
		hasRev.put("Message", "All Data");
		hasRev.put("Total", listRevDto.size());
		hasRev.put("Data", listRevDto);
	return hasRev;
	}
	
//	Melihat reviewer by Id
	@GetMapping("/reviewers/{id}")
	public HashMap<String, Object> getReviewerById(@PathVariable(value="id") int revId) {
		HashMap<String, Object> hasRev = new HashMap<String, Object>();
		
		Reviewer rev = reviewerRepository.findById(revId).orElse(null);
		
		ReviewerDTO revDto = convertEntityToDTO(rev);
		
		hasRev.put("Message", "Data By Id");
		hasRev.put("Data", revDto);
	return hasRev;
	}
	
//	Menambahkan seorang reviewer
	@PostMapping("/reviewers/add")
	public HashMap<String, Object> addReviewer(@Valid @RequestBody ReviewerDTO reviewerDTO) {
		HashMap<String, Object> hasRev = new HashMap<String, Object>();
		
		Reviewer rev = convertDTOToEntity(reviewerDTO);
		
		hasRev.put("Message", "Add success");
		hasRev.put("Data", reviewerRepository.save(rev));
	return hasRev;
	}
	
//	Menghapus director
	@DeleteMapping("/reviewers/delete/{id}")
	public HashMap<String, Object> deleteReviewerById (@PathVariable(value="id") int revId) {
		HashMap<String, Object> hasRev = new HashMap<String, Object>();
		
		Reviewer rev = reviewerRepository.findById(revId).orElse(null);
		
		reviewerRepository.delete(rev);
		hasRev.put("Message", "Delete success");
		hasRev.put("Data", rev);
	return hasRev;
	}
	
//	Update data
	@PutMapping("/reviewers/update/{id}")
	public HashMap<String, Object> updateReviewers (@PathVariable(value="id") int revId, @Valid @RequestBody ReviewerDTO reviewerDTO) {
		HashMap<String, Object> hasRev = new HashMap<String, Object>();
		
		Reviewer rev = reviewerRepository.findById(revId).orElse(null);
		
		reviewerDTO.setRevId(rev.getRevId());
		
		rev.setRevName(convertDTOToEntity(reviewerDTO).getRevName());
		
		hasRev.put("Message" , "Update success");
		hasRev.put("Data", reviewerRepository.save(rev));	
	return hasRev; 
	}
 }
