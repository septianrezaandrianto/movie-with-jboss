package moviejbossreza.generateentities.controller;

import java.util.ArrayList;
import java.util.HashMap;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import mov.moviejbossreza.dto.GenresDTO;
import moviejbossreza.generateentities.Genres;
import moviejbossreza.generateentities.repository.GenresRepository;

@RestController
@RequestMapping("/api")
public class GenresController {

	@Autowired
	private GenresRepository genresRepository;
	
	ModelMapper modelMapper = new ModelMapper();
	
//	Convert Entity To DTO
	public GenresDTO convertEntityToDTO(Genres genres) {
		GenresDTO genDto = modelMapper.map(genres, GenresDTO.class);
	return genDto;	
	}
	
//	Convert DTO To Entity
	public Genres convertDTOToEntity(GenresDTO genresDTO) {
		Genres gen = modelMapper.map(genresDTO, Genres.class);
	return gen;
	}
	
//	Melihat semua list genres
	@GetMapping("/genres/all")
	public HashMap<String, Object> getAllGenres() {
		
		HashMap<String, Object> hasGen = new HashMap<String, Object>();
		ArrayList<GenresDTO> listGenDto = new ArrayList<GenresDTO>();
		
		for (Genres gen : genresRepository.findAll()) {
			GenresDTO genDto = convertEntityToDTO(gen);
			
			listGenDto.add(genDto);
		}
		hasGen.put("Message", "All Data");
		hasGen.put("Total", listGenDto.size());
		hasGen.put("Data", listGenDto);
	return hasGen;
	}
	
//	Melihat sebuah genre berdasarkan id
	@GetMapping("/genres/{id}")
	public HashMap<String, Object> getGenresById(@PathVariable(value ="id") int genId) {
		HashMap<String, Object> hasGen = new HashMap<String, Object>();
		
		Genres gen = genresRepository.findById(genId).orElse(null);
		
		GenresDTO genDto = convertEntityToDTO(gen);
		
		hasGen.put("Message", "Data By Id");
		hasGen.put("Data", genDto);
	return hasGen;
	}
	
//	Menambahkan sebuah genre
	@PostMapping("/genres/add")
	public HashMap<String, Object> addGenres(@Valid @RequestBody GenresDTO genresDTO) {
		HashMap<String, Object> hasGen = new HashMap<String, Object>();
		Genres gen = convertDTOToEntity(genresDTO);
		hasGen.put("Message", "Add Success");
		hasGen.put("Data", genresRepository.save(gen));
	return hasGen;
	}
	
//	Menghapus sebuah genre
	@DeleteMapping("/genres/delete/{id}")
	public HashMap<String, Object> deleteGenreById(@PathVariable(value="id") int genId) {
		HashMap<String, Object> hasGen = new HashMap<String, Object>();
		Genres gen = genresRepository.findById(genId).orElse(null);
		genresRepository.delete(gen);
		
		hasGen.put("Message", "Delete Success");
		hasGen.put("Data", gen);
	return hasGen;	
	}
	
//	Mengupdate sebuah genre
	@PutMapping("/genres/update/{id}")
	public HashMap<String, Object> updateGenre(@PathVariable(value="id") int genId, @Valid @RequestBody GenresDTO genresDTO) {
		
		HashMap<String, Object> hasGen = new HashMap<String, Object>();		
		Genres gen = genresRepository.findById(genId).orElse(null);
		genresDTO.setGenId(gen.getGenId());		
		gen.setGenTitle(convertDTOToEntity(genresDTO).getGenTitle());
		
		hasGen.put("Message", "Update Success");
		hasGen.put("Data", genresRepository.save(gen));
	return hasGen;
	}
	
	
}
