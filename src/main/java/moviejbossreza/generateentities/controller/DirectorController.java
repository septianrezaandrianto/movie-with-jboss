package moviejbossreza.generateentities.controller;

import java.util.ArrayList;
import java.util.HashMap;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import mov.moviejbossreza.dto.DirectorDTO;
import moviejbossreza.generateentities.Director;
import moviejbossreza.generateentities.repository.DirectorRepository;

@RestController
@RequestMapping("/api")
public class DirectorController {

	@Autowired
	private DirectorRepository directorRepository;
	
	ModelMapper modelMapper = new ModelMapper();
	
//	Convert Entity To DTO
	public DirectorDTO convertEntityToDTO(Director director) {
		DirectorDTO dirDto = modelMapper.map(director, DirectorDTO.class);
	return dirDto;
	}
	
//	Convert DTO To Entity
	public Director convertDTOToEntity(DirectorDTO directorDTO) {
		Director dir = modelMapper.map(directorDTO, Director.class);
	return dir;
	}
	
//	Melihat list Director
	@GetMapping("/directors/all")
	public HashMap<String, Object> getAllDirector() {
		
		HashMap<String, Object> hashMapDirector = new HashMap<String, Object>();
		ArrayList<DirectorDTO> listDirDto = new ArrayList<DirectorDTO>();
		
		for (Director dir : directorRepository.findAll()) {
			listDirDto.add(convertEntityToDTO(dir));
		}
		hashMapDirector.put("Message", "All Data");
		hashMapDirector.put("Total", listDirDto.size());
		hashMapDirector.put("Data", listDirDto);
		
	return hashMapDirector;
	}

//	Melihat Director berdasarkan id
	@GetMapping("/directors/{id}")
	public HashMap<String, Object> getDirectorById(@PathVariable(value="id") int dirId) {
		
		HashMap<String, Object> hashMapDirector = new HashMap<String, Object>();
		
		Director dir = directorRepository.findById(dirId).orElse(null);
							// Mapping dengan model mapper	
		DirectorDTO dirDto = convertEntityToDTO(dir);
		
		hashMapDirector.put("Message" , "Data By Id");
		hashMapDirector.put("Data", dirDto);
		
	return hashMapDirector;
	}
	
//	Menambahkan seorang director
	@PostMapping("/directors/add")
	public HashMap<String, Object> addDirector(@Valid @RequestBody DirectorDTO directorDTO) {
		
		HashMap<String, Object> hashMapDirector = new HashMap<String, Object>();
//		Mapping dengan model mapper	
		Director director = convertDTOToEntity(directorDTO);
	
		hashMapDirector.put("Message", "Add success");
		hashMapDirector.put("Data", directorRepository.save(director));
		
	return hashMapDirector;
	}
	
//	Menghapus Director
	@DeleteMapping("/directors/delete/{id}")
	public HashMap<String, Object> deleteDirectorById(@PathVariable(value="id") int dirId) {
		
		HashMap<String, Object> hashMapDirector = new HashMap<String, Object>();
		
		Director dir = directorRepository.findById(dirId).orElse(null);
		
		directorRepository.delete(dir);
		
		hashMapDirector.put("Message", "Delete success");
		hashMapDirector.put("Data",dir);
		
	return hashMapDirector;
	}
	
//	Update data Directors
	@PutMapping("/directors/update/{id}")
	public HashMap<String, Object> updateDirector(@PathVariable(value="id") int dirId, @Valid @RequestBody DirectorDTO directorDTO) {
		
		HashMap<String, Object> hashMapDirector = new HashMap<String, Object>();
		
		Director dir = directorRepository.findById(dirId).orElse(null);
				
		directorDTO.setDirId(dir.getDirId());
		
//		Mapping dengan model mapper	
		if (directorDTO.getDirFname() != null) {
			dir.setDirFname(convertDTOToEntity(directorDTO).getDirFname());
		}
		if (directorDTO.getDirLname() != null) {
			dir.setDirLname(convertDTOToEntity(directorDTO).getDirLname());
		}	
		
		hashMapDirector.put("Message", "Update success");
		hashMapDirector.put("Data", directorRepository.save(dir));
		
	return hashMapDirector;
	}
	
}
